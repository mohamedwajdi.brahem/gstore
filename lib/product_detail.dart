import 'package:flutter/material.dart';

class ProductDetail extends StatelessWidget {
  const ProductDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(10, 20, 10, 40),
          child: Image.asset("assets/images/dmc5.jpg"),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 20, 10, 60),
          child: const Text(
            '''Un texte est une série orale ou écrite de mots perçus comme constituant un ensemble cohérent, porteur de sens et utilisant les structures propres à une langue (conjugaisons, construction et association des phrases…). ... L'étude formelle des textes s'appuie sur la linguistique,qui est l'approche scientifique du langage.
            ''',
            maxLines: 10,
          ),
        ),
        const Text(
          "200 TND",
          textScaleFactor: 3,
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 60, 10, 60),
          child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              minimumSize: Size(200, 50),
            ),
            onPressed: () {},
            icon: Icon(Icons.shopping_basket_rounded),
            label: const Text(
              "Acheter",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    );
  }
}
