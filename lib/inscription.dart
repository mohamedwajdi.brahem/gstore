import 'package:flutter/material.dart';

class Inscription extends StatelessWidget {
  const Inscription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Container(
        margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
        child: Image.asset("assets/images/dmc5.jpg"),
      ),
      Container(
        margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'UserName',
          ),
        ),
      ),
      Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Email',
          ),
        ),
      ),
      Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Mot de passe',
          ),
        ),
      ),
      Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Année de naissance',
          ),
        ),
      ),
      Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
        child: const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Adresse de facturation',
          ),
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(onPressed: () {}, child: const Text("S'inscrire")),
          const SizedBox(width: 20),
          ElevatedButton(onPressed: () {}, child: const Text("Annuler")),
        ],
      )
    ]);
  }
}
