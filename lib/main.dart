import 'package:flutter/material.dart';
import 'package:hello_world/product_detail.dart';
import 'package:hello_world/product_info.dart';

import 'inscription.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'G-Store ESPRIT',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title:
                Title(color: Colors.black, child: const Text("G-Store ESPRIT")),
          ),
          body: Column(
            children: const [
              Inscription(),

              //ProductDetail()
              /*
              ProductInfo(
                  title: "Devil May Cry 5",
                  price: 200,
                  image: "assets/images/dmc5.jpg"),
              ProductInfo(
                  title: "Resident Evil VIII",
                  price: 200,
                  image: "assets/images/re8.jpg"),
              ProductInfo(
                  title: "Need For speed Heat",
                  price: 100,
                  image: "assets/images/nfs.jpg"),
              ProductInfo(
                  title: "Red Dead Redemption II",
                  price: 150,
                  image: "assets/images/rdr2.jpg"),
              ProductInfo(
                title: "Fifa 22",
                price: 100,
                image: "assets/images/fifa.jpg",
              ),
              ProductInfo(
                  title: "Minecraft",
                  price: 200,
                  image: "assets/images/minecraft.jpg")
                  */
            ],
          )),
    );
  }
}
