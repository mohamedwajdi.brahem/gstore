import 'package:flutter/material.dart';

class ProductInfo extends StatelessWidget {
  final String image;
  final String title;
  final int price;

  //const ProductInfo(this._title, this._price, this._image);
  const ProductInfo(
      {Key? key, required this.title, required this.price, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                margin: const EdgeInsets.all(10),
                child: Image.asset(image, width: 200, height: 94)),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title),
                Text(
                  price.toString() + "TND",
                  textScaleFactor: 2,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
